## 安装程序说明
安装程序用于方便项目部署。

### 使用步骤

* 将安装程序jrelax-setup以war的形式依赖到项目中
* 删除安装程序中的 .locked 文件
* 将项目的数据库初始化文件放入 install/database 中，并命名为：mysql.sql

启动项目将会出现安装界面